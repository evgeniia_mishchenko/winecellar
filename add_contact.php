<?php include('core/init.php'); ?>

<?php
$db = new Database;
$db->query('INSERT INTO wines (name, year, grapes, country, region, description, picture)
              VALUES(:name, :year, :grapes, :country, :region, :description, :picture)');
$db->bind(':name', $_POST['name']);
$db->bind(':year', $_POST['year']);
$db->bind(':grapes', $_POST['grapes']);
$db->bind(':country', $_POST['country']);
$db->bind(':region', $_POST['region']);
$db->bind(':description', $_POST['description']);
$db->bind(':picture', $_POST['picture']);

if($db->execute()){
    echo 'Wine was added';
} else {
    echo 'Could not add wine details';
}
?>

