<?php include('core/init.php'); ?>

<?php
    $db = new Database;
    $db->query('SELECT * FROM wines');
    $wines = $db->resultset();

?>
            <!-- $contacts is an object that why we use arrow notation-->
        <div class="row">

            <?php foreach($wines as $wine) : ?>

                    <div id="here" class="large-3 columns">
                        <div style="width:100%; height: 60%;background: #fff;position:relative;">
                            <img src="img/<?php echo $wine->picture; ?>" style="position:absolute;height:90%;width:auto;left:1%;margin-top:5%;">
                        </div>
                        <a href="#">
                            <div><?php echo $wine->name; ?></div>
                            <div><?php echo $wine->year; ?></div>
                            <div> <?php echo $wine->grapes; ?></div>
                            <div> <?php echo $wine->country; ?>, <?php echo $wine->region; ?></div>
                        </a>
                        <ul class="button-group right">
                            <li>
                                <a href="" class="button tiny edit-btn" data-reveal-id="editModal<?php echo $wine->id; ?>">Edit</a>
                                <div id="editModal<?php echo $wine->id; ?>" class="reveal-modal editModal" data-reveal>
                                    <div class="row">
                                        <div class="large-8 columns">
                                            <h2 class="text-center">Edit Wine Details</h2>
                                            <form method="POST" action="" id="editWine">
                                            <div class="row">
                                                <div class="small-8 large-centered columns">
                                                    <label>Name
                                                        <input name="name" type="text" placeholder="" value="<?php echo $wine->name; ?>">
                                                    </label>
                                                    <label>Year
                                                        <input name="year" type="text" placeholder="" value="<?php echo $wine->year; ?>">
                                                    </label>
                                                    <label>Grapes
                                                        <input name="grapes" type="text" placeholder="" value="<?php echo $wine->grapes; ?>">
                                                    </label>
                                                    <label>Country
                                                        <select name="country">
                                                             <?php foreach($countries as $key=>$value) : ?>
                                                                    <?php if ($key == $wine->country) {
                                                                        $selected = 'selected';
                                                                    } else {
                                                                        $selected = '';
                                                                    }
                                                                    ?>
                                                                    <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
                                                             <?php endforeach; ?>
                                                        </select>
                                                    </label>
                                                    <label>Region
                                                        <input name="region" type="text" placeholder="" value="<?php echo $wine->region; ?>">
                                                    </label>
                                                    <label>Description
                                                        <textarea name="description" placeholder="" rows="7"> <?php echo $wine->description; ?></textarea>
                                                    </label>
                                                    <label>Picture
                                                        <input name="picture" type="text" placeholder="" value="<?php echo $wine->picture; ?>">
                                                    </label>
                                                    <input type="hidden" name="id" value="<?php echo $wine->id; ?>"/>
                                                    <input type="submit" name="submit" class="edit-btn button float-right small" value="Edit">
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                        <div class="large-3 columns" style="margin-top: 160px;"><img src="img/<?php echo $wine->picture; ?>"></div>
                                        <div class="large-1 columns"></div>
                                        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <form action="#" method="POST" id="deleteWine">
                                    <input type="hidden" name="id" value="<?php echo $wine->id; ?>">
                                    <input type="submit" name="submit" class="delete-btn button tiny" value="Delete">
                                </form>
                            </li>
                        </ul>

                    </div>
            <?php endforeach; ?>
        </div>






