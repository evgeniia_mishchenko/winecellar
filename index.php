<!-- It is SPA so we wont use include header cause we dont have a banch of files-->
<?php include('core/init.php'); ?>

<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Ajax Wine Cellar</title>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="js/vendor/modernizr.js"></script>

</head>
<body>
<div data-magellan-expedition="fixed" >
    <dl class="sub-nav">
        <dd data-magellan-arrival="build"><a href="#build">Build with HTML</a></dd>
        <dd data-magellan-arrival="js"><a href="#js">Arrival 2</a></dd>
    </dl>
</div>
<div class="row">

    <div class="large-6 columns">
        <h1><strong>SPA <span style="color:#F2AB22;">WineCellar</strong></span></h1>
    </div>
    <div class="large-6 columns">
        <a class="add-btn button right small" data-reveal-id="addModal" ><strong>Add Wine</strong></a>
        <div class="reveal-modal" id="addModal" data-reveal>
            <h1 class="text-center" style="color:#000;">Add Wine</h1>
            <form method="POST" action="add_contact.php" id="addWine">
                <div class="row">
                    <div class="small-8 large-centered columns">
                        <label>Name
                            <input name="name" type="text" placeholder="">
                        </label>
                        <label>Year
                            <input name="year" type="text" placeholder="">
                        </label>
                        <label>Grapes
                            <input name="grapes" type="text" placeholder="">
                        </label>
                        <label>Country
                            <select name="country" value="select">
                                <option>Select Country</option>
                                <?php foreach($countries as $key=>$value) : ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </label>
                        <label>Region
                            <input name="region" type="text" placeholder="">
                        </label>
                        <label>Description
                            <textarea name="description" placeholder="" ></textarea>
                        </label>
                        <label>Picture
                            <input name="picture" type="text" placeholder="">
                        </label>
                        <input type="submit" name="submit" class="add-btn button float-right small" value="Add">
                    </div>
                </div>
            </form>
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>

        </div>
    </div>
</div>

<!-- Loading Image -->
<div id="loaderImage">
    <img src="img/ajax-loader.gif">
</div>
<!-- Main Content -->
<div id="pageContent">

</div>
<footer id="footer">
    <div class="row">
        <div class="large-12 columns" style="margin-top: 30px;">
            <div class="large-6 columns">
                <div class="copyrights float-left" style="margin-left:9px;">Copyright 2015 | <a href="index.php">WineCellar </a></div>
            </div>
            <div class="large-6 columns social-ul">
                <ul class="inline-list right">
                    <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li class="social-youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    <li class="social-rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div><!-- End social-ul -->
        </div>
    </div><!-- End container -->
</footer><!-- End footer -->
<script src="js/vendor/jquery.js"></script>
<script src="js/script.js"></script>
<script src="js/foundation.min.js"></script>
<script>
    $(document).foundation();
</script>
<!-- script for Upload file custom input-->
<script>
    !function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)
</script>

</body>
</html>
