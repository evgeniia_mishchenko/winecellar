$(document).ready(function(){
    $('#loaderImage').show();
    showContacts();

    //Add wine
    $(document).on('submit', '#addWine', function(){
        $('#loaderImage').show();
        //Post data from form
        $.post("add_contact.php", $(this).serialize())
            .done(function(data){
                console.log(data);
                $('#addModal').foundation('reveal', 'close');
                showContacts();
            });
        return false;
    });

    //Edit wine
    $(document).on('submit', '#editWine', function(){
        $('#loaderImage').show();
        //Post data from form
        $.post("edit_contact.php", $(this).serialize())
            .done(function(data){
                console.log(data);
                $('.editModal').foundation('reveal', 'close');
                showContacts();
            });
        return false;
    });

    //Delete wine
    $(document).on('submit', '#deleteWine', function(){
        $('#loaderImage').show();
        //Post data from form
        $.post("delete_contact.php", $(this).serialize())
            .done(function(data){
                console.log(data);
                showContacts();
            });
        return false;
    });
});
function showContacts(){
    //console.log('Contacts..');
    setTimeout("$('#pageContent').load('contacts.php', function(){$('#loaderImage').hide();});", 1000);
}

