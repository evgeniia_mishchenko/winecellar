<?php
//First we create our db class. PDO is best practice for connecting to mysql DB using PHP. It is much safer.
// U can use prepared statements which obviously add a lot of security, prevents sql injection(����� �� ���� ������)...
// with PDO we can use quite a few different databases not just mysql
// He borrowed PDO PHP class from Culttt.com programming blog (http://culttt.com/2012/10/01/roll-your-own-pdo-php-class/   2012)


//this class is very simple but its also reliable and relatively secure
class Database{
    private $host      = DB_HOST;   //this const will be set in a config file
    private $user      = DB_USER;
    private $pass      = DB_PASS;
    private $dbname    = DB_NAME;

    private $dbh;       //our handler so we can interact with DB
    private $error;
    private $stmt;      //we can use for our prepared statements

    public function __construct(){
        // Set DSN
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        // Set options
        $options = array(       //there is tone of different options we can use with PDO(php.net)
            PDO::ATTR_PERSISTENT    => true,        //so we just wonna have a persistent connection(����������)= we can have one connection and multiple interactions with it
            PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION
        );
        // Create a new PDO instance
        try{
            //its just a connect string
            //we create new PDO object and passing in our credentials
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
            // Catch any errors
        catch(PDOException $e){
            $this->error = $e->getMessage();
        }//����� �� �� ����� ����� ������ �����, ��������� ������ ������ ������� �����.
    }

    //query function will create a statement for us
    //we pass a query from wherever we are working(SELECT * FROM topics) and then we create a statement with that

    public function query($query){
        $this->stmt = $this->dbh->prepare($query);
    }
    //and then we gonna bind values with this statement
    //so that we prevent sql injections
    //all we do here is checking the type of input (some validation on it)
    public function bind($param, $value, $type = null){
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }
    //its just gonna execute whatever your statement is
    public function execute(){
        return $this->stmt->execute();
    }

    public function resultset(){
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);//instead we can specify FETCH_ASSOC and work with it
    }
    //the same as resultSet but it will be one result
    public function single(){
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }
    //amount of rows returned
    public function rowCount(){
        return $this->stmt->rowCount();
    }

    public function lastInsertId(){
        return $this->dbh->lastInsertId();
    }

    //TRANSACTIONS
    public function beginTransaction(){
        return $this->dbh->beginTransaction();
    }

    public function endTransaction(){
        return $this->dbh->commit();
    }

    public function cancelTransaction(){
        return $this->dbh->rollBack();
    }
}
//we have our DB class created, which is basically a wrapper class because
//we gonna create subclasses for topics, users etc.and this class gonna be wrapper for different kind of interactions
//but before we will create template class which is really important - when writing good php code u wonna separate
//your views from logic, libraries, decision making ..its not a good practice to generate html inside of a class



