<?php include('core/init.php'); ?>

<?php
$db = new Database;
$db->query('UPDATE wines SET
              name = :name,
              year = :year,
              grapes = :grapes,
              country =:country,
              region = :region,
              description = :description,
              picture = :picture
              WHERE id=:id');
$db->bind(':name', $_POST['name']);
$db->bind(':year', $_POST['year']);
$db->bind(':grapes', $_POST['grapes']);
$db->bind(':country', $_POST['country']);
$db->bind(':region', $_POST['region']);
$db->bind(':description', $_POST['description']);
$db->bind(':picture', $_POST['picture']);
$db->bind(':id', $_POST['id']);


if($db->execute()){
    echo 'Wine details were updated';
} else {
    echo 'Could not update wine details';
}
?>

